const mysql = require('mysql');

const connections = require('./connections')
const ddl = require('./ddl')
const dml = require('./sdml')
const dsl = require('./dsl')

benchmarkAll()

function benchmarkAll() {
  Object.entries(connections).reverse().forEach(
    ([ hoster, config ]) => benchmark(hoster, config)
  )
}

async function benchmark(hoster, config) {
    const connectLabel = `${hoster} connected in:`
    console.time(connectLabel)

    const connection = mysql.createConnection(config)

    connection.connect();
    console.timeEnd(connectLabel)

    const run = query => new Promise((resolve, reject) => connection.query(query, (error, results) => {
        error ? reject(error) : resolve(results)
    }))

    try {
        const ddlLabel = `DDL (${hoster}:${ddl.length}) done in:`
        console.time(ddlLabel)
        for(let query of ddl) {
            await run(query)
        }
        console.timeEnd(ddlLabel)

        const dmlLabel = `DML (${hoster}:${dml.length}) done in:`
        console.time(dmlLabel)
        for(let query of dml) {
            await run(query)
        }
        console.timeEnd(dmlLabel)

        const dslLabel = `DSL (${hoster}:${dsl.length}) done in:`
        console.time(dslLabel)
        for(let query of dsl) {
            await run(query)
        }
        console.timeEnd(dslLabel)

        await run('drop table album,alert,captcha,comment,file,filetype,`group`,invite,menu,message,news,peuchat,photo,post,remind,subject,topic,user;')
    } catch (error) {
        console.log(error)
    } finally {
        connection.end();
    }
}

function wait(timeout) {
    return new Promise(
        resolve => setTimeout(resolve, timeout)
    )
}


// CREATE USER 'remote_user2'@'%' IDENTIFIED VIA mysql_native_password USING "Vjz,fpfg455";
// GRANT SELECT, INSERT, UPDATE, DELETE, CREATE, DROP, FILE, INDEX, ALTER, CREATE TEMPORARY TABLES, CREATE VIEW, EVENT, TRIGGER, SHOW VIEW, CREATE ROUTINE, ALTER ROUTINE, EXECUTE ON *.* TO 'remote_user2'@'%' REQUIRE NONE WITH MAX_QUERIES_PER_HOUR 0 MAX_CONNECTIONS_PER_HOUR 0 MAX_UPDATES_PER_HOUR 0 MAX_USER_CONNECTIONS 0;GRANT ALL PRIVILEGES ON `test_db`.* TO 'remote_user2'@'%';
