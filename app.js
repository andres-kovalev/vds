const express = require('express');
const mysql = require('mysql');

const { vds } = require('./connections')
const ddl = require('./ddl')
const dml = require('./sdml')
const dsl = require('./dsl')

const PORT = 80;

const app = express();

app.get('/', async (req, res) => {
    const connection = mysql.createConnection(vds)
    connection.connect()

    const run = query => new Promise((resolve, reject) => connection.query(query, (error, results) => {
        error ? reject(error) : resolve(results)
    }))

    // await run('drop table album,alert,captcha,comment,file,filetype,`group`,invite,menu,message,news,peuchat,photo,post,remind,subject,topic,user;')

    const result = {
        ddl: Date.now()
    }

    for(let query of ddl) {
        await run(query)
    }

    Object.assign(result, {
        ddl: Date.now() - result.ddl,
        dml: Date.now()
    })

    for (let query of dml) {
        await run(query)
    }

    Object.assign(result, {
        dml: Date.now() - result.dml,
        dsl: Date.now()
    })

    for (let query of dsl) {
        await run(query)
    }

    result.dsl = Date.now() - result.dsl

    res.status(200).json(result)
});

app.listen(PORT, () => {
    console.log(`served on ${ PORT }...`);
});
